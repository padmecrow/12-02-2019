function doubleNum(num) {
    return num * 2;
}
function double(num)  {
    if (typeof num !== 'number') {
        throw new Error('input need to be int!');
    }
    return doubleNum(num);
}
module.exports = double;