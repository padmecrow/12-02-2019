const replicate = require('./replicate');

describe('replicate', function() {
    it('"x" times 1', function() {
        expect(replicate('x', 1)).toBe('x');
    });
    it('"x" times 2', function() {
        expect(replicate('x', 2)).toBe('xx');
    });
    it('"x" times 3', function() {
        expect(replicate('x', 3)).toBe('xxx');
    });
    it('"x" times 10', function() {
        expect(replicate('x', 10)).toBe('xxxxxxxxxx');
    });
    it('"x" times -1', function() {
        expect(replicate('x', -1)).toBe('');
    });
    it('"x" times 2.4', function() {
        expect(function () {
            replicate('x', 2.4);
        }).toThrow('times have to be int!');
    });
    it('new Date() times 3', function() {
        expect(function () {
            replicate(new Date(), 3);
        }).toThrow('word is not string!');
    });
});
