const double = require('./double');

describe('double', function() {
    it('double(3) = 6', function() {
        expect(double(3)).toBe(6);
    });
    it('double(0) = 0', function() {
        expect(double(0)).toBe(0);
    });
    it('error "cat" is string', function() {
        expect(function() {
            double('cat'); 
        }).toThrow('input need to be int!');
    });
});
